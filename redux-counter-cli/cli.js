const readline = require('readline');
const Redux = require('redux');

const initialState = 0;

function counter(state = initialState, action) {
    switch (action.type) {
        case 'INCR':
            return state + 1;
        case 'DECR':
            return state - 1;
        case 'EXIT':
            return NaN;
        default:
            return state;
    }
}

const store = Redux.createStore(counter);
const unsubscribe = store.subscribe(function() {
    const counter = store.getState();
    if (Number.isNaN(counter)) {
        process.exit();
    }
    console.log(`=> ${counter.toString()}`);
});

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false,
});
rl.on('line', function(line) {
    const action = {type: line.trim().toUpperCase()};
    store.dispatch(action);
});

console.log('Commands: INCR / DECR / EXIT');
